#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 23 13:19:07 2018
@Project: Mosaic Ordering Scheme 
    Read .img files from each months directory. Find the highest frequency date that makes the mosaic and make it
    parent. The sub-parent will then be the date closest to max frequency. The trend will continue going down the 
    hierarchy till the last tile. The last tile will be at the bottom.

@author: Siddharth Shankar
"""

import glob, os
import datetime as dt
from collections import Counter
import matplotlib.pyplot as plt
import numpy as np
import time
from itertools import groupby
from math import floor
import collections
import os

path =  r"/cresis/snfs1/scratch/sshankar/VertexASF/Paper1/AGU2019/Sep2016-1/"
vrtName = 'Sep2016-4.vrt'
tifName = 'Sep2016-4.tif'
ordertxt = 'orderSep2016-4.txt'
def formMosaic(monthDir):
    #Read .img files from each BEAM-DIMAP file format directories
    
    dateList = []
    orderList = []
    keyList = []
    fileDict = {} #create dictionary key value pair key = decimal date, value = path to .img file
    dirs = os.listdir(os.path.join(path,monthDir))
    for dir in dirs:
        if dir.endswith(".data"):
            datesStr = str(dir).split("_",5)[4]
            date,_,tstmp =  datesStr.rpartition('T')
            day = (float(tstmp[-6:-4])+(float(tstmp[-4:-2])/60)+(float(tstmp[-2:])/3600))/24 #Date includes timestamp --> time is converted into days which is decimal value
            dateList.append(float(date[-2:])+day)

            fileDict[float(date[-2:])+day] = (glob.glob(path+monthDir+"/"+dir+"/"+"*Sigma0_HH.img")) #Assiging value(.img path) to key(decimal date)
            print (fileDict)
#            fileList.append((glob.glob(path+monthDir+"/"+dir+"/"+"*.img")))
    with open("/cresis/snfs1/scratch/sshankar/VertexASF/Paper1/AGU2019/Sep2016-1/All_processed4/orderSep2016-4.txt",mode ="w") as f:
        
        for key,value in sorted(fileDict.items()):
            print (key,value)
            orderList.append(value)
            keyList.append(key)
            
            f.write(("%s\n"% (str(value)).strip("[]")).replace("'",""))
    print ("Orders",orderList)
    print ("Keys",keyList)
    list_ = [floor(x) for x in keyList]
    print (list_)
    counter = collections.Counter(list_)
    #Use this and bottom one os.system()
    os.system("gdalbuildvrt -input_file_list /cresis/snfs1/scratch/sshankar/VertexASF/Paper1/AGU2019/Sep2016-1/All_processed4/orderSep2016-4.txt /cresis/snfs1/scratch/sshankar/VertexASF/Paper1/AGU2019/Sep2016-1/All_processed4/Sep2016-4.vrt -srcnodata 0")
    startTime = time.time()
    os.system("gdalbuildvrt -input_file_list /cresis/snfs1/scratch/sshankar/VertexASF/Paper1/AGU2019/Sep2016-1/All_processed4/orderSep2016-4.txt /cresis/snfs1/scratch/sshankar/VertexASF/Paper1/AGU2019/Sep2016-1/All_processed4/Sep2016-4.vrt -srcnodata 0 & gdal_translate -co TILED=YES -co BLOCKXSIZE=10240 -co BLOCKYSIZE=10240 /cresis/snfs1/scratch/sshankar/VertexASF/Paper1/AGU2019/Sep2016-1/All_processed4/Sep2016-4.vrt /cresis/snfs1/scratch/sshankar/VertexASF/Paper1/AGU2019/Sep2016-1/All_processed4/Sep2016-4.tif ")
    
    endTime =  time.time()
    
    '''
    Enable os.system(), when running a new tile ordering feature.
    '''
#    os.system("gdal_translate -co TILED=YES -co BLOCKXSIZE=10240 -co BLOCKYSIZE=10240 order.vrt tiled10240_order.tif")
    
    print ("Total time to generate vrt and geotiff %s seconds" % (endTime - startTime))
    

formMosaic("All_processed4")
