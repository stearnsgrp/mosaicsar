#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 23 13:19:07 2018
@Project: Mosaic Ordering Scheme 
    Read .img files from each months directory. Find the highest frequency date that makes the mosaic and make it
    parent. The sub-parent will then be the date closest to max frequency. The trend will continue going down the 
    hierarchy till the last tile. The last tile will be at the bottom.
    
    Overview is added to the mosaicking process. ~110 GB size overview is generated for the entire Greenland mosaic.
    
    Nodata value has been changed from 0 to -9999 as we deal with real number data range ~100<=pixel<=50.
    Including 0 will not work well as probable iceberg pixels can be masked out as well and can result in nodata values
    within the raster itself.
    
@author: Siddharth Shankar
"""

import glob, os
import datetime as dt
from collections import Counter
import matplotlib.pyplot as plt
import numpy as np
import time
from itertools import groupby
from math import floor
import collections
import os
import rasterio
from rasterio.merge import merge


path =  r"/cresis/snfs1/scratch/sshankar/VertexASF/Paper1/2017-2019/"
# path = r"/cresis/snfs1/scratch/sshankar/VertexASF/Paper1/AGU2019/Sep2016-1/"
os.chdir(path)
print(os.getcwd())


vrtName = 'Jan2017_v4_-9999.vrt'
tifName = 'Jan2017_v4_-9999.tif'
orderTxt = 'orderJan2017_v4_-9999.txt'
'''
os.system() arguments for virtual raster, mosaic, and overview raster creation
'''
readobj = []
def formMosaic(monthDir):
    #Read .img files from each BEAM-DIMAP file format directories
    # os.mkdir(monthDir)
    dateList = []
    orderList = []
    keyList = []
    fileDict = {} #create dictionary key value pair key = decimal date, value = path to .img file
    orderListMatch = []
    dirs = os.listdir(os.path.join(path,monthDir))
    for dir in dirs:
        if dir.endswith(".data"):
            datesStr = str(dir).split("_",5)[4]
            date,_,tstmp =  datesStr.rpartition('T')
            day = (float(tstmp[-6:-4])+(float(tstmp[-4:-2])/60)+(float(tstmp[-2:])/3600))/24 #Date includes timestamp --> time is converted into days which is decimal value
            dateList.append(float(date[-2:])+day)

            fileDict[float(date[-2:])+day] = (glob.glob(path+monthDir+"/"+dir+"/"+"*Sigma0_HH_db.img")) #Assiging value(.img path) to key(decimal date)

    with open("""%s%s/%s"""
              %(path,monthDir,orderTxt),mode ="w") as f:
        
        for key,value in sorted(fileDict.items()):
            print (key,value)
            orderList.append(value[0])
            keyList.append(key)
            
            f.write(("%s\n"% (str(value)).strip("[]")).replace("'",""))
    '''
    for num,file_ in enumerate(orderList):
        file_ = match_histograms(file_[num],file_[0])
        orderListMatch.append(file_)
    '''
    print(orderListMatch)
    list_ = [floor(x) for x in keyList]
    counter = collections.Counter(list_)
    startTime = time.time()
    
    print("VRT file creation")
    
    
    os.system(("""gdalbuildvrt -srcnodata "0" -vrtnodata "-9999" -a_srs "EPSG:3413" -input_file_list %s%s/%s %s%s/%s""")#-hidenodata -vrtnodata -9999    -srcnodata 0""" -srcnodata "0 nan"
               %(path,monthDir,orderTxt,path,monthDir,vrtName))
    
    print("VRT to Tiff file creation")
    os.system(("""gdal_translate -r bilinear -of GTiff -a_srs "EPSG:3413" -co TILED=YES -co BIGTIFF=YES -co BLOCKXSIZE=10240 -co BLOCKYSIZE=10240 %s%s/%s %s%s/%s""")#-a_nodata "0 nan" 
                 %(path,monthDir,vrtName,path,monthDir,tifName))

    
    print("Overview creation from .tif file")
    os.system(("""gdaladdo -ro --config BIGTIFF_OVERVIEW YES %s%s/%s 2 4 8 16""")%(path,monthDir,tifName))    
    
    
    endTime =  time.time()
    print ("Total time to generate vrt and geotiff %s seconds" % (endTime - startTime))
    
formMosaic("Jan2017-1-v3")
