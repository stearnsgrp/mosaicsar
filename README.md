# MOSAIC SAR Greenland Ice Sheet

Layering the sar images based on a specific order.
ENV = CFAR3

Mosaic Border noise removed

![Border Noise Removal](https://bitbucket.org/sid07/mosaicsar/raw/c9a3bbd8d250656c2596fa58cda2e1a73fe1827a/img/mosaicNoBrdrNoise.png)

Mosaic sample of Greenland coverage upto 150 km

![Mosaic Greenland](https://bitbucket.org/sid07/mosaicsar/raw/c9a3bbd8d250656c2596fa58cda2e1a73fe1827a/img/GrIS_mosaic.png)